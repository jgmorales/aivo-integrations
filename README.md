# Very short description of the package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/aivo/integration-complements.svg?style=flat-square)](https://packagist.org/packages/aivo/integration-complements)
[![Build Status](https://img.shields.io/travis/aivo/integration-complements/master.svg?style=flat-square)](https://travis-ci.org/aivo/integration-complements)
[![Quality Score](https://img.shields.io/scrutinizer/g/aivo/integration-complements.svg?style=flat-square)](https://scrutinizer-ci.com/g/aivo/integration-complements)
[![Total Downloads](https://img.shields.io/packagist/dt/aivo/integration-complements.svg?style=flat-square)](https://packagist.org/packages/aivo/integration-complements)

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what PSRs you support to avoid any confusion with users and contributors.

## Installation

You can install the package via composer:

```bash
composer require aivo/integrations
```

## Usage

``` php
// Usage description here
```

### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email jmorales@aivo.co instead of using the issue tracker.

## Credits

- [Jose Morales](https://github.com/aivo)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
