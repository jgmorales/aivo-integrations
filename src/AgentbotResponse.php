<?php
/**
 * Created by PhpStorm.
 * User: jose
 */

namespace Aivo\Integrations;

use Aivo\Integrations\Complements\Complement;

class AgentbotResponse implements \JsonSerializable
{
    /**
     * Answer to be returned to the chat
     * @var string
     */
    private $answer;
    
    
    /**
     * Complements to be returned to the chat
     * @var array
     */
    private $complements = [];
    
    /**
     * AgentbotResponse constructor.
     * @param string $answer
     * @param Complement $complement
     * @param Complement[] $complements
     */
    public function __construct(string $answer = '', Complement $complement = null, Complement ...$complements)
    {
        $this->answer = $answer;
        if (!empty($complement)) {
            $this->complements[] = $complement->getComplement();
        }
        foreach ($complements as $singleComplement) {
            $this->complements[] = $singleComplement->getComplement();
        }
    }
    
    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'answer' => $this->answer,
            'answer_clean' => strip_tags($this->answer),
            'complements' => $this->complements,
        ];
    }
}
