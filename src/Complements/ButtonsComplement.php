<?php
/**
 * Created by PhpStorm.
 * User: Jose
 */

namespace Aivo\Integrations\Complements;

/**
 * Class Buttons
 */
class ButtonsComplement extends Complement
{
    
    /**
     * Button ACTION
     */
    const ACTION = 'buttons';
    
    /**
     * @var bool
     */
    private $blockInput;
    
    /**
     * @var bool
     */
    private $oneChoice;
    
    /**
     * @var bool
     */
    private $quickReplies;
    
    /**
     * @var bool
     */
    private $isNextStep;

    /**
     * @var array
     */
    public $data = [];
    
    /**
     * Buttons constructor.
     *
     * @param boolean $blockInput
     * @param boolean $oneChoice
     * @param boolean $quickReplies
     */
    public function __construct(bool $blockInput = false, bool $oneChoice = false, bool $quickReplies = false)
    {
        parent::__construct(self::ACTION);
        $this->setBlockInput($blockInput);
        $this->setOneChoice($oneChoice);
        $this->setQuickReplies($quickReplies);
        $this->isNextStep = false;
    }

    /**
     * @param string $label
     * @param string $type
     * @param string $value
     * @return void
     */
    public function addButton(string $label, string $type, string $value)
    {
        if ($type == 'next_step') {
            $this->isNextStep = true;
            
            $this->data[] = [
                "label" => $label,
                "next_step" => $value,
            ];
        } else {
            $this->data[] = [
                "label" => $label,
                "type" => $type,
                "value" => $value,
            ];
        }
    }

    /**
     * @return array
     */
    public function getComplement()
    {
        return [
            "action" => $this->getAction(),
            "blockInput" => $this->getBlockInput(),
            "oneChoice" => $this->getOneChoice(),
            "quickReplies" => $this->getQuickReplies(),
            "param" => $this->getData(),
        ];
    }
    
    /**
     * @return string
     */
    public function getAction()
    {
        if ($this->isNextStep) {
            return 'button';
        }
        return parent::getAction();
    }
    
    /**
     * @return array
     */
    public function getData()
    {
        if ($this->isNextStep) {
            return $this->data;
        } else {
            return [
                "data" => $this->data,
            ];
        }
    }

    /**
     * @return mixed
     */
    private function getBlockInput()
    {
        return $this->blockInput;
    }

    /**
     * @param boolean $blockInput
     * @return void
     */
    private function setBlockInput(bool $blockInput)
    {
        $this->blockInput = $blockInput;
    }

    /**
     * @return mixed
     */
    private function getOneChoice()
    {
        return $this->oneChoice;
    }

    /**
     * @param boolean $oneChoice
     * @return void
     */
    private function setOneChoice(bool $oneChoice)
    {
        $this->oneChoice = $oneChoice;
    }

    /**
     * @return mixed
     */
    private function getQuickReplies()
    {
        return $this->quickReplies;
    }

    /**
     * @param boolean $quickReplies
     * @return void
     */
    private function setQuickReplies(bool $quickReplies)
    {
        $this->quickReplies = $quickReplies;
    }
}
