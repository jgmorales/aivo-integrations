<?php

namespace Aivo\Integrations\Complements;

/**
 * Class Change Url Complement
 */
class ChangeUrlComplement extends Complement
{
    /**
     * Change Url Complement Action
     */
    const ACTION = 'change_url';

    /**
     * ChangeUrlComplement constructor.
     * @param string $url
     */
    public function __construct(string $url)
    {
        parent::__construct(self::ACTION);

        if (!empty($url)) {
            $this->setParam((string)$url);
        } else {
            throw new \InvalidArgumentException('Url cannot be empty or null');
        }
    }

    /**
     * @return array
     */
    public function getComplement()
    {
        return [
            "action" => $this->getAction(),
            "param" => $this->getParam(),
        ];
    }
}
