<?php
/**
 * Created by PhpStorm.
 * User: jose
 */

namespace Aivo\Integrations\Complements;

abstract class Complement
{
    /**
     * @var string
     */
    private $action = '';

    /**
     * @var mixed
     */
    private $param = '';
    
    /**
     * @var string
     */
    private $nextStep = '';
    
    /**
     * BaseComplements constructor.
     *
     * @param string $action
     */
    public function __construct(string $action)
    {
        $this->setAction($action);
    }
    
    /**
     * @return string
     */
    public function getNextStep()
    {
        return $this->nextStep;
    }
    
    /**
     * @param string $nextStep
     * @return void
     */
    public function setNextStep(string $nextStep)
    {
        $this->nextStep = $nextStep;
    }
    
    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }
    
    /**
     * @param string $action
     * @return void
     */
    public function setAction(string $action)
    {
        $this->action = $action;
    }

    /**
     * @param string|integuer $param
     * @return void
     */
    public function setParam($param)
    {
        $this->param = $param;
    }

    /**
     * @return array
     */
    public function getParam()
    {
        return $this->param;
    }
}
