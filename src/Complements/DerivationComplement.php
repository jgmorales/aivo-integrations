<?php
/**
 * Created by PhpStorm.
 * User: Jose
 */

namespace Aivo\Integrations\Complements;

/**
 * Class Derivation Complement
 *
 */
class DerivationComplement extends Complement
{
    /**
     * Derivation Complement Action
     */
    const ACTION = 'derivation';

    /**
     * DerivationComplement constructor.
     * @param mixed $derivationId
     */
    public function __construct($derivationId)
    {
        parent::__construct(self::ACTION);

        if (!empty($derivationId)) {
            $this->setParam((int)$derivationId);
        } else {
            throw new \InvalidArgumentException('Derivation Id cannot be empty or null');
        }
    }

    /**
     * @return array
     */
    public function getComplement()
    {
        return [
            "action" => $this->getAction(),
            "param" => $this->getParam(),
        ];
    }
}
