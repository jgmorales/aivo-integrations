<?php
/**
 * Created by PhpStorm.
 * User: jose
 */

namespace Aivo\Integrations\Complements;

/**
 * Class Image Complement
 */
class ImageComplement extends Complement
{
    /**
     * Image Complement Action
     */
    const ACTION = 'image';
    
    /**
     * ImageComplement constructor.
     * @param string $param
     */
    public function __construct(string $param)
    {
        parent::__construct(self::ACTION);
        
        if (!empty($param)) {
            $this->setParam($param);
        } else {
            throw new \InvalidArgumentException('Image url cannot be empty or null');
        }
    }

    /**
     * @return array
     */
    public function getComplement()
    {
        return [
            "action" => $this->getAction(),
            "param" => $this->getParam(),
        ];
    }
}
