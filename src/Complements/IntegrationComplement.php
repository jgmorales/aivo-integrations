<?php
/**
 * Created by PhpStorm.
 * User: Jose
 */

namespace Aivo\Integrations\Complements;

/**
 * Class Integration Complement
 */
class IntegrationComplement extends Complement
{
    /**
     * Integration Complement Action
     */
    const ACTION = 'integration';

    /**
     * IntegrationComplement constructor.
     * @param string $integrationName
     */
    public function __construct(string $integrationName)
    {
        parent::__construct(self::ACTION);

        if (!empty($integrationName)) {
            $this->setParam((string)$integrationName);
        } else {
            throw new \InvalidArgumentException('Integration name cannot be empty or null');
        }
    }

    /**
     * @return array
     */
    public function getComplement()
    {
        return [
            "action" => $this->getAction(),
            "param" => $this->getParam(),
        ];
    }
}
