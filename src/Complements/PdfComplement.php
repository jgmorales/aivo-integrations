<?php

namespace Aivo\Integrations\Complements;

/**
 * Class PDF
 */
class PdfComplement extends Complement
{
    /**
     * PDF Complement Action
     */
    const ACTION = 'file';

    /**
     * PdfComplement constructor.
     * @param string $param
     */
    public function __construct(string $param)
    {
        parent::__construct(self::ACTION);

        if (!empty($param)) {
            $this->setParam((string)$param);
        } else {
            throw new \InvalidArgumentException('File url cannot be empty or null');
        }
    }

    /**
     * @return array
     */
    public function getComplement()
    {
        return [
            "action" => $this->getAction(),
            "param" => $this->getParam(),
        ];
    }
}
