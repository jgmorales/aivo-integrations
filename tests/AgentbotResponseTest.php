<?php

namespace Aivo\IntegrationComplements\Tests;

use Aivo\Integrations\AgentbotResponse;
use Aivo\Integrations\Complements\Image;
use PHPUnit\Framework\TestCase;

class AgentbotResponseTest extends TestCase
{
    /** @test */
    public function AgentbotResponseReturnAExpectedResponse()
    {
        $response = new AgentbotResponse('This is your answer');
        
        $this->assertArrayHasKey('answer', $response->jsonSerialize());
        $this->assertArrayHasKey('answer_clean', $response->jsonSerialize());
        $this->assertArrayHasKey('complements', $response->jsonSerialize());
        
    }
    
    /** @test */
    public function AgentbotResponseThrowsAnExceptionWhenNonStringIsPassedAsAnAnswer()
    {
        $this->expectException(\TypeError::class);
        new AgentbotResponse([]);
    }
    
    /** @test */
    public function AgentbotResponseThrowsAnExceptionWhenNonComplementInstanceIsPassed()
    {
        $this->expectException(\TypeError::class);
        new AgentbotResponse('Answer', []);
    }
    
    /** @test */
    public function AgentbotResponseReturnsExpectedResponseWithOneComplement()
    {
        $imageComplement = new Image('Url to image');
        $response = new AgentbotResponse('This is your answer', $imageComplement);
    
        // Convert to array
        $response = $response->jsonSerialize();
        
        $this->assertArrayHasKey('answer', $response);
        $this->assertArrayHasKey('answer_clean', $response);
        $this->assertArrayHasKey('complements', $response);
        $this->assertEquals('This is your answer', $response['answer']);
        $this->assertIsArray($response['complements']);
        $this->assertEquals(1, sizeof($response['complements']));
        $this->assertArrayHasKey('action', $response['complements'][0]);
        $this->assertArrayHasKey('param', $response['complements'][0]);
        $this->assertEquals('image', $response['complements'][0]['action']);
        $this->assertEquals('Url to image', $response['complements'][0]['param']);
    }
    
    /** @test */
    public function AgentbotResponseReturnsExpectedResponseWithSeveralComplements()
    {
        $imageComplement1 = new Image('Url to image');
        $imageComplement2 = new Image('Url to image');
        $imageComplement3 = new Image('Url to image');
        $response = new AgentbotResponse(
            'This is your answer',
            $imageComplement1,
            $imageComplement2,
            $imageComplement3
        );
        // Convert to array
        $response = $response->jsonSerialize();
        
        $this->assertArrayHasKey('answer', $response);
        $this->assertArrayHasKey('answer_clean', $response);
        $this->assertArrayHasKey('complements', $response);
        $this->assertEquals('This is your answer', $response['answer']);
        $this->assertIsArray($response['complements']);
        // Expect 3 complements
        $this->assertEquals(3, sizeof($response['complements']));
        // Complement 1 is image
        $this->assertArrayHasKey('action', $response['complements'][0]);
        $this->assertArrayHasKey('param', $response['complements'][0]);
        $this->assertEquals('image', $response['complements'][0]['action']);
        $this->assertEquals('Url to image', $response['complements'][0]['param']);
        // TODO: Complement 2 and 3
    }
}
